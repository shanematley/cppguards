" =============================================================================
" File:         autoload/cppguards.vim
" Description:  A script to insert header guards, namespaces and copyright
"               messages in C++ files.
" Author:       Shane Matley <shane@matley.com.au>
" =============================================================================
"
" INSTALLATION:
"
" To install, place this file in the autoload directory and then add the
" following to your .vimrc file:
"
"   nnoremap <leader>h :call CppHeaderGuards_InsertCppHeaderData()<cr>
"   nnoremap <leader>H :call CppHeaderGuards_InsertCppSourceData()<cr>
"
" To have this script automatically add the information when creating header
" and source files add this to your .vimrc file:
"
"   let g:cppguards_auto_on_file_creation = 1
"   call CppHeaderGuards_Setup()
"
" OPTIONS:
"
" * To insert a copyright message at the top of files set (Default: None):
"
"   let g:cppguards_copyright = "Copyright message\<CR>..."
"
" * To change the default header file extension (Default: h):
"
"   let g:cppguards_header_ext = "hpp"
"
" * By default standard double-quotes are used to add include files. To use a
"   system include style instead (angle-brackets) set the following:
"
"   let g:cppguards_use_system_include_style = 1
"
" * By default header guards use a single underscore between directories and
"   before the file extension. To use two instead set the following:
"
"   let g:cppguards_use_double_underscores = 1
"
" * By default add header/source information is something you need to request
"   (e.g. by adding the key mappings above and using them). This can be
"   automated on new C++ file creation by setting:
"
"   let g:cppguards_auto_on_file_creation = 1
"
if exists("g:loaded_cppguards") || &cp
    finish
endif

let g:loaded_cppguards = 1

if !exists('g:cppguards_copyright') | let g:cppguards_copyright = "" | en
if !exists('g:cppguards_header_ext') | let g:cppguards_header_ex = 'h' | en
if !exists('g:cppguards_use_system_include_style') | let g:cppguards_use_system_include_style = 0 | en
if !exists('g:cppguards_use_double_underscores') | let g:cppguards_use_double_underscores = 0 | en
if !exists('g:cppguards_auto_on_file_creation') | let g:cppguards_auto_on_file_creation = 0 | en
if !exists('g:cppguards_no_mappings') | let g:cppguards_no_mappings = 0 | en

" Creates a header guard variable from the path of the current file. The path used
" is the full path with the current working directory stripped from it.
" E.g. Given:
"  - Working directory: /home/me/repo
"  - Current file: /home/me/repo/Company/Project/MyDirectory/MyFile.hpp
" Then running s:GetHeaderGuardsFromPath(0) would result in:
"  - COMPANY_PROJECT_MY_DIRECTORY_MY_FILE_HPP
" Whereas running s:GetHeaderGuardsFromPath(1) would result in double
" underscores between directories and before the extension:
"  - COMPANY__PROJECT__MY_DIRECTORY__MY_FILE__HPP
function! s:GetHeaderGuardsFromPath(use_double_for_directories)
    let l:subpath = substitute(expand("%:p"), getcwd()."/", "", "g")
    if a:use_double_for_directories
        let l:header_guard_double = substitute(l:subpath, '/', "_", "g")
        let l:header_guard_double = substitute(l:header_guard_double, '\.', "__", "g")
        return toupper(substitute(l:header_guard_double, '[A-Z][a-z]', '_&', "g"))
    else
        let l:header_guard_single = substitute(l:subpath, "[/.]", "_", "g")
        return toupper(substitute(l:header_guard_single, '[^_]\@<=[A-Z][a-z]', '_&', "g"))
    endif
endfunction

" Creates a namespace string from the path of the current file. The path used
" is the full path with the current working directory stripped from it.
" E.g. Given:
"  - Working directory: /home/me/repo
"  - Current file: /home/me/repo/Company/Project/MyDirectory/MyFile.h
" Then the namespace would be:
"  - namespace Company { namespace Project { namespace MyDirectory {
function! s:GetNamespaceFromPath()
    let l:directory = substitute(expand("%:p:h"), getcwd(), "", "g")
    let l:ns_open = substitute(l:directory, '/\([^/]\+\)', ' namespace \1 {', "g")
    return substitute(l:ns_open, '^ ', '', "")
endfunction

if !exists('g:Cppguards_ns_builder') | let g:Cppguards_ns_builder = function("s:GetNamespaceFromPath") | en

function! s:GetClosingNamespace(opening_ns)
    return substitute(a:opening_ns, 'namespace [^{]\+{ \?', '}', "g") . " // " . a:opening_ns
endfunction

function! CppHeaderGuards_InsertCppHeaderData()
    let l:header_guard = s:GetHeaderGuardsFromPath(g:cppguards_use_double_underscores)
    let l:ns_open = g:Cppguards_ns_builder()
    let l:ns_close = s:GetClosingNamespace(l:ns_open)

    let l:top =
            \"#ifndef " . l:header_guard . "\<CR>" .
            \"#define " . l:header_guard . "\<CR>" .
            \"\<CR>" .
            \l:ns_open . "\<CR>\<CR>"

    let l:bottom =
            \l:ns_close . "\<CR>" .
            \"\<CR>" .
            \"#endif // " . l:header_guard

    set paste
    execute "normal! m`gg"
    if strlen(g:cppguards_copyright) > 0
        execute "normal! i" . g:cppguards_copyright . "\<CR>"
    endif
    execute "normal! i" . l:top
    execute "normal! Go\<CR>" . l:bottom
    execute "normal! ``"
    set nopaste
endfunction

" Inserts namespace, copyright and header include into C++ source file.
function! CppHeaderGuards_InsertCppSourceData()
    let l:header_guard = s:GetHeaderGuardsFromPath(g:cppguards_use_double_underscores)
    let l:ns_open = g:Cppguards_ns_builder()
    let l:ns_close = s:GetClosingNamespace(l:ns_open)
    let l:header_file = substitute(expand("%:p:r"), getcwd()."/", "", "g") . "." . g:cppguards_header_ex
    if g:cppguards_use_system_include_style
        let l:header_file = "<" . l:header_file . ">"
    else
        let l:header_file = '"' . l:header_file . '"'
    endif


    let l:top =
            \"#include " . l:header_file . "\<CR>" .
            \"\<CR>" .
            \l:ns_open . "\<CR>\<CR>"

    let l:bottom = l:ns_close

    set paste
    execute "normal! m`gg"
    if strlen(g:cppguards_copyright) > 0
        execute "normal! i" . g:cppguards_copyright . "\<CR>"
    endif
    execute "normal! i" . l:top
    execute "normal! Go\<CR>" . l:bottom
    execute "normal! ``"
    set nopaste
endfunction

if g:cppguards_auto_on_file_creation
    autocmd BufNewFile *.{H,h,hxx,hpp,ipp,ixx,txx,tpp} call CppHeaderGuards_InsertCppHeaderData()
    autocmd BufNewFile *.{C,cc,cpp,cxx} call CppHeaderGuards_InsertCppSourceData()
endif

if ! g:cppguards_no_mappings
    nnoremap <leader>h :call CppHeaderGuards_InsertCppHeaderData()<cr>
    nnoremap <leader>H :call CppHeaderGuards_InsertCppSourceData()<cr>
endif
